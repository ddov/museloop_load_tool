
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;


namespace Tool
{

    public class ChooseExporter : Exporter
    {

        private static int PUZZLE_SHEET = 9;
        private static string QUESTION_PREFIX = "q";
        private static string OVERLAY_PREFIX = "a";
        private static string POPUP_PREFIX = "p";
        private static string ICON = "icon";

        public ChooseExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, Constants.CHOOSE_TABLE)
        {
        }

        protected override int GetSheetNumber()
        {
            return PUZZLE_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            string IDAttr = Utils.getString(entry, "dbid");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = getIcons(entry);
            if (null != value)
                item["Icons"] = value;
            value = getRounds(entry);
            if (null != value)
                item["Rounds"] = value;
            value = Utils.getIntAttributeValue(entry, "minsteps");
            if (null != value)
                item["MinSteps"] = value;
            value = Utils.getIntAttributeValue(entry, "stepsfoul");
            if (null != value)
                item["StepsFoul"] = value;
            return item;
        }

        private AttributeValue getRounds(JObject entry)
        {
            List<AttributeValue> layerList = new List<AttributeValue>();
            int layerIndex = 1;
            while (isLayerValid(entry, layerIndex))
            {
                AttributeValue layer = new AttributeValue();
                AttributeValue question = Utils.getMapAttributeValue(entry, QUESTION_PREFIX + layerIndex);
                AttributeValue popup = Utils.getMapAttributeValue(entry, POPUP_PREFIX + layerIndex);
                AttributeValue answers = Utils.getNodeJsCompatibleSetAttributeValue(entry, OVERLAY_PREFIX + layerIndex);
                if (null != question)
                    layer.M.Add("Question", question);
                if (null != popup)
                    layer.M.Add("Popup", popup);
                if (null != answers)
                    layer.M.Add("Answers", answers);
                layerList.Add(layer);
                layerIndex++;
            }
            AttributeValue allLayers = new AttributeValue();
            allLayers.L = layerList;
            return allLayers;
        }

        public AttributeValue getIcons(JObject entry)
        {
            AttributeValue[] icons = new AttributeValue[4];
            for (int i = 1; i <= icons.Length; i++)
            {
                icons[i - 1] = Utils.getStringAttributeValue(entry, ICON + i);
            }
            AttributeValue value = new AttributeValue();
            value.L = new List<AttributeValue>(icons);
            return value;
        }

        private bool isLayerValid(JObject entry, int index)
        {
            string question = Utils.getString(entry, QUESTION_PREFIX + index + ".he");
            if (null == question || question.Trim().Length <= 1)
                return false;
            return true;
        }

        protected override bool isValid(JObject entry)
        {
            string IDAttr = Utils.getString(entry, "dbid");
            if (mIdSet.Contains(IDAttr))
            {
                throw new Exception(String.Format("DUPLICATE ID: {0}. Check out the " +
                        "Excel file to see there are duplicates in dbid. Aborting!!!!!", IDAttr));
            }
            return base.isValid(entry);
        }

        public override string Name => "Choose";
    }
}