

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;


namespace Tool
{
    public class MuseumExporter : Exporter
    {

        public MuseumExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, Constants.MUSEUM_TABLE, "ID")
        {
        }

        protected override int GetSheetNumber()
        {
            return 1;
        }


        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            String IDAttr = Utils.getString(entry, "id");
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getStringAttributeValue(entry, "backgroundimage");
            if (null != value)
                item["BackgroundImage"] = value;
            value = Utils.getMapAttributeValue(entry, "name");
            if (null != value)
                item["Name"] = value;
            value = Utils.getFloatAttributeValue(entry, "latitude");
            if (null != value)
                item["Latitude"] = value;
            value = Utils.getFloatAttributeValue(entry, "longitude");
            if (null != value)
                item["Longitude"] = value;
            value = Utils.getMapAttributeValue(entry, "city");
            if (null != value)
                item["City"] = value;
            value = Utils.getIntAttributeValue(entry, "radius");
            if (null != value)
                item["Radius"] = value;
            value = Utils.getStringAttributeValue(entry, "logo");
            if (null != value)
                item["Logo"] = value;
            value = Utils.getMapAttributeValue(entry, "info");
            if (null != value)
                item["Info"] = value;
            value = Utils.getIntAttributeValue(entry, "giftscore");
            if (null != value)
                item["GiftScore"] = value;
            value = Utils.getIntAttributeValue(entry, "maxgifts");
            if (null != value)
                item["MaxGifts"] = value;
            value = Utils.getListAttributeValue(entry, "languages");
            if (null != value)
                item["Languages"] = value;
            return item;
        }

        protected override bool isValid(JObject entry)
        {
            String museumId = Utils.getString(entry, "id");
            return ((null != museumId) && (museumId.Trim().Length > 0));
        }

        public override string Name => "Museum";
    }
}
