﻿using System;
using System.Runtime.Serialization;

namespace Tool { 

    [Serializable]
    internal class ExporterException : Exception
    {
        public string Key
        {
            get; set;
        }


        public ExporterException(string message, Exception innerException) : base()
        {
            Key = message;
        }
    }
}