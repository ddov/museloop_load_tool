﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tool;
using unirest_net.http;

namespace LoadTool
{
    abstract class MultipleExporter
    {
        Dictionary<int, String> mHosts = new Dictionary<int, string>();
        private string mTargetTable;
        private AmazonDynamoDBClient mClient;
        public static bool INTEGRATION = true;
        public string HOST = "https://spreadsheets.google.com/feeds/list/{0}/{1}/public/values?alt=json";
        private string mMuseumId;
        public int BATCH_MAX_ITEM_COUNT = 20;



        public MultipleExporter(MuseumEntry entry, AmazonDynamoDBClient client, string targetTable)
        {
            this.mClient = client;
            this.mTargetTable = (INTEGRATION) ? (targetTable + "Integration") : targetTable;

            foreach (int sheetNumber in GetSheetNumbers())
            {
                String host = String.Format(HOST,
                    entry.GoogleDocID,
                    sheetNumber);
                    mHosts.Add(sheetNumber, host);
            }
            mMuseumId = entry.ID;
        }



        protected abstract HashSet<int> GetSheetNumbers();

        public void execute()
        {

            Console.WriteLine("executing multiple exporter " + this.GetType().ToString());
            List<Dictionary<string, AttributeValue>> items = generateItemList();
            int listSize = items.Count;
            int batches = (listSize / BATCH_MAX_ITEM_COUNT) + 1;
            for (int i = 0; i < batches; i++)
            {
                var howMuchSent = i * BATCH_MAX_ITEM_COUNT;
                var howMuchLeft = listSize - howMuchSent;
                List<Dictionary<string, AttributeValue>> sublist = items.GetRange(howMuchSent, Math.Min(howMuchLeft, BATCH_MAX_ITEM_COUNT));
                sendBatch(sublist);
            }
            
        }

        public void sendBatch(List<Dictionary<string, AttributeValue>> items)
        {
            Dictionary<string, List<WriteRequest>> requestItems = new Dictionary<string, List<WriteRequest>>();
            List<WriteRequest> list = new List<WriteRequest>();
            requestItems[mTargetTable] = list;
            foreach (Dictionary<string, AttributeValue> item in items)
            {
                list.Add(new WriteRequest(new PutRequest(item)));
            }
            if (list.Count > 0)
            {
                BatchWriteItemResponse response = mClient.BatchWriteItem(new BatchWriteItemRequest(requestItems));
                if (response.UnprocessedItems.Count > 0)
                {
                    Thread.Sleep(5000);
                    BatchWriteItemResponse secondBatch = mClient.BatchWriteItem(new BatchWriteItemRequest(response.UnprocessedItems));
                    if (secondBatch.UnprocessedItems.Count > 0)
                    {
                        throw new Exception("Could not handle all items in batch. Try again later.");
                    }
                }
                Console.WriteLine(response);
            }
            else
            {
                Console.WriteLine("empty list");
            }
        }

        private List<Dictionary<string, AttributeValue>> generateItemList()
        {
            Dictionary<int, List<object>> map = new Dictionary<int, List<object>>();
            foreach (KeyValuePair<int, string> entry in mHosts)
            {
                HttpResponse<string> jsonResponse = Unirest.get(entry.Value)
                    .header("accept", "application/json").asJson<string>();
                var root = JObject.Parse(jsonResponse.Body);
                JObject feed = (JObject)root["feed"];
                JArray entryArray = (JArray)feed["entry"];
                List<object> localItems = getItemList(entry.Key, entryArray);
                map.Add(entry.Key, localItems);
            }

            List<Dictionary<string, AttributeValue>> items = createAWSElements(map);
            return items;
        }

        protected abstract List<object> getItemList(int sheetNumber, JArray entry);

        protected abstract List<Dictionary<string, AttributeValue>> createAWSElements(Dictionary<int, List<object>> itemLists);

        protected virtual bool isValid(JObject entry)
        {
            String museumId = Utils.getString(entry, "museumid");
            return ((null != museumId) && (museumId.Trim().Length > 0));
        }
    }
}
