
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Tool
{
    public class PuzzleExporter : Exporter
    {

        private static int PUZZLE_SHEET = 7;

        public PuzzleExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, Constants.PUZZLE_TABLE)
        {
        }

        protected override int GetSheetNumber()
        {
            return PUZZLE_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            string IDAttr = Utils.getString(entry, "dbid");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getStringAttributeValue(entry, "artjpg");
            if (null != value)
                item["Art"] = value;
            value = Utils.getIntAttributeValue(entry, "rowscount");
            if (null != value)
                item["RowsCount"] = value;
            value = Utils.getIntAttributeValue(entry, "columnscount");
            if (null != value)
                item["ColumnsCount"] = value;
            value = Utils.getIntAttributeValue(entry, "minsteps");
            if (null != value)
                item["MinSteps"] = value;
            value = Utils.getIntAttributeValue(entry, "stepsfoul");
            if (null != value)
                item["StepsFoul"] = value;
            value = Utils.getMapAttributeValue(entry, "p1");
            if (null != value)
                item["Popup"] = value;
            return item;
        }

        protected override bool isValid(JObject entry)
        {
            string IDAttr = Utils.getString(entry, "dbid");
            if (mIdSet.Contains(IDAttr))
            {
                throw new System.Exception(String.Format("DUPLICATE ID: %s. Check out the " +
                        "Excel file to see there are duplicates in dbid. Aborting!!!!!", IDAttr));
            }
            return base.isValid(entry);
        }

        public override string Name => "Puzzle";
    }
}