

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using Tool;
using unirest_net.http;

public abstract class Exporter {

    private readonly AmazonDynamoDBClient mClient;
    private readonly string mTargetTable;
    private readonly string mHost;
    private readonly string mMuseumId;
    private string mQueryId = "MuseumID";
    private JObject mNode;
    protected HashSet<string> mIdSet = new HashSet<string>();
    private Dictionary<string, string> sIds = new Dictionary<string, string>();

    public int BATCH_MAX_ITEM_COUNT = 20;

    public static bool INTEGRATION = true;
    public string HOST = "https://spreadsheets.google.com/feeds/list/{0}/{1}/public/values?alt=json";


    public Exporter(MuseumEntry entry, 
        AmazonDynamoDBClient client, 
        string targetTable) : this(entry, client, targetTable, "MuseumID") {
        
    }

    public Exporter(MuseumEntry entry, AmazonDynamoDBClient client, string targetTable, string queryId) {
        this.mClient = client;
        this.mTargetTable = (INTEGRATION) ? (targetTable + "Integration") : targetTable;
        mHost = String.Format(HOST,
                entry.GoogleDocID,
                GetSheetNumber());
        mMuseumId = entry.ID;
        mQueryId = queryId;
    }

    protected abstract int GetSheetNumber();


    public void execute() {

        Console.WriteLine("executing exporter " + this.GetType().ToString());
    //    if (!(this is GamesExporter)) {
    //        Deleter deleter = new Deleter(mClient, mTargetTable, mMuseumId, mQueryId);
    //        deleter.execute();
    //    }
  
        HttpResponse<string> jsonResponse = Unirest.get(mHost)
                .header("accept", "application/json").asJson<string>();

        mNode = JObject.Parse(jsonResponse.Body);
        JObject root = mNode;
        JObject feed = (JObject)root["feed"]; 
        JArray entryArray = (JArray)feed["entry"];
        List<Dictionary<string, AttributeValue>> items = getItemList(entryArray);
        int listSize = items.Count;
        int batches = (listSize / BATCH_MAX_ITEM_COUNT) + 1;
        increateProvisionedThroughput();
        try {
            for (int i = 0; i < batches; i++) {
                var howMuchSent = i * BATCH_MAX_ITEM_COUNT;
                var howMuchLeft = listSize - howMuchSent;
                List<Dictionary<string, AttributeValue>> sublist = items.GetRange(howMuchSent, Math.Min(howMuchLeft, BATCH_MAX_ITEM_COUNT));
                sendBatch(sublist);
            }
        } finally {
            decreaseProvisinedThroughput();
        }
    }

    private void increateProvisionedThroughput() {
        //mClient.updateTable(mTargetTable, new ProvisionedThroughput().withWriteCapacityUnits(20L).withReadCapacityUnits(20L));
    }

    private void decreaseProvisinedThroughput() {
        //mClient.updateTable(mTargetTable, new ProvisionedThroughput().withWriteCapacityUnits(2L).withReadCapacityUnits(10L));
    }


    public List<Dictionary<string, AttributeValue>> getItemList(JArray array) {
        mIdSet.Clear();
        List<Dictionary<string, AttributeValue>> list = new List<Dictionary<string, AttributeValue>>();
        for (int i = 0; i < array.Count; i++) {
            JObject entry = (JObject)array[i];
            if (!isValid(entry))
                continue;

            try
            {
                Dictionary<string, AttributeValue> item = jsonToAws(entry);
                list.Add(item);
            }
            catch (Exception ex)
            {
                ex.Data.Add(Constants.ERROR_SHEET, Name);
                throw ex;
            }
            
        }
        return list;
    }

    protected abstract Dictionary<string,AttributeValue> jsonToAws(JObject entry);

    /**
     * should be no more than 20, to prevent it from failing.
     * @param items
     */
    public void sendBatch(List<Dictionary<string, AttributeValue>> items) {
        Dictionary<string, List<WriteRequest>> requestItems = new Dictionary<string, List<WriteRequest>>();
        List<WriteRequest> list = new List<WriteRequest>();
        requestItems[mTargetTable] = list;
        foreach (Dictionary<string, AttributeValue> item in items) {
            list.Add(new WriteRequest(new PutRequest(item)));
        }
        if (list.Count > 0) {
            BatchWriteItemResponse response = mClient.BatchWriteItem(new BatchWriteItemRequest(requestItems));
            if (response.UnprocessedItems.Count > 0)
            {
                Thread.Sleep(5000);
                BatchWriteItemResponse secondBatch = mClient.BatchWriteItem(new BatchWriteItemRequest(response.UnprocessedItems));
                if (secondBatch.UnprocessedItems.Count > 0)
                {
                    throw new Exception("Could not handle all items in batch. Try again later.");
                }
            }
            Console.WriteLine(response);
        } else {
            Console.WriteLine("empty list");
        }
    }

    protected virtual bool isValid(JObject entry) {
        String museumId = Utils.getString(entry, "museumid");
        return ((null != museumId) && (museumId.Trim().Length > 0));
    }

    public virtual string Name
    {
        get
        {
            return "NONE";
        }
    }
}
