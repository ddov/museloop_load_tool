
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;


namespace Tool
{
    public class StationExporter : Exporter
    {

        private static int STATIONS_SHEET = 4;


        public StationExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, Constants.STATION_TABLE)
        {
        }

        protected override int GetSheetNumber()
        {
            return STATIONS_SHEET;
        }

        protected override bool isValid(JObject entry)
        {
            String IDAttr = Utils.getString(entry, "dbid");
            if (mIdSet.Contains(IDAttr))
            {
                throw new Exception(String.Format("DUPLICATE ID: {0}. Check out the " +
                        "Excel file to see there are duplicates in dbid. Aborting!!!!!", IDAttr));
            }
            return base.isValid(entry);
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            String IDAttr = Utils.getString(entry, "dbid");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getMapAttributeValue(entry, "anecdote");
            if (null != value)
                item["Anecdote"] = value;
            value = Utils.getMapAttributeValue(entry, "artistname");
            if (null != value)
                item["ArtistName"] = value;
            value = Utils.getMapAttributeValue(entry, "readmore");
            if (null != value)
                item["ExtraInfo"] = value;
            value = Utils.getMapAttributeValue(entry, "artworkname");
            if (null != value)
                item["Name"] = value;
            value = Utils.getStringAttributeValue(entry, "stationiconjpg");
            if (null != value)
                item["ProfileImage"] = value;
            value = Utils.getStringAttributeValue(entry, "gametype");
            if (null != value)
                item["Type"] = value;
            value = Utils.getMapAttributeValue(entry, "directions");
            if (null != value)
                item["Directions"] = value;
            value = Utils.getStringAttributeValue(entry, "artworkyear");
            if (null != value)
                item["Year"] = value;
            return item;
        }


        public override string Name => "Station";
    }

}