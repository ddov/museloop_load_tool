﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using Tool;

namespace LoadTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, UpdateControl
    {
        public MainWindow()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            MuseumDatabase database = LoadMuseumFromXML();
            InitDesign(database);
        }


        private MuseumDatabase LoadMuseumFromXML()
        {

            XmlSerializer serializer = new XmlSerializer(typeof(MuseumDatabase));
            FileStream fs = null;
            MuseumDatabase museums = null;
            try
            {
                fs = new FileStream(Constants.MUSEUM_SREIALZED_FILE, FileMode.Open);
                museums = (MuseumDatabase)serializer.Deserialize(fs);
            }
            catch (FileNotFoundException)
            {
                museums = new MuseumDatabase();
            }
            if (null != fs)
                fs.Close();
            return museums;

        }

        private void InitDesign(MuseumDatabase museums)
        {
            tabController.SelectedIndex = 0;

            museumComboBox.ItemsSource = museums;
            museumComboBox.DisplayMemberPath = "Name";

            museumListView.SelectionMode = SelectionMode.Single;
            museumListView.ItemsSource = museums;
            GridView gView = museumListView.View as GridView;

        }


        private async void Execute(object sender, RoutedEventArgs e)
        {
            outputTextBox.Clear();
            bool isIntegration = false;

            if (null != integrationCheckBox.IsChecked) //check for a value  
            {
                isIntegration = (bool)integrationCheckBox.IsChecked;
            }
            Exporter.INTEGRATION = isIntegration;
            MuseumEntry museum = (MuseumEntry)museumComboBox.SelectedValue;
            Executor executor = new Executor();
            Task<bool> task = Task.Run(() => executor.CommunicateDynamoDb(museum, this));
            deleteButton.IsEnabled = false;
            loadButton.IsEnabled = false;
            AppendTextBox("Running...\n");
            bool result = await task;
            if (result)
            {
                AppendTextBox("All went OK\n");
            }
            else
            {
                AppendTextBox(executor.ErrorMsg + "\n");
            }
            deleteButton.IsEnabled = true;
            loadButton.IsEnabled = true;

        }

        private async void Delete(object sender, RoutedEventArgs e)
        {
            MuseumEntry museum = (MuseumEntry)museumComboBox.SelectedValue;
            bool integration = false;
            if (integrationCheckBox.IsChecked != null)
            {
                integration = (bool)integrationCheckBox.IsChecked;
            }

            MessageBoxResult warningResult = MessageBox.Show(
                "Are you sure you want to delete " + museum.Name + "???", "", MessageBoxButton.YesNo);

            if (warningResult == MessageBoxResult.No)
            {
                return;
            }

            outputTextBox.Clear();

            Executor executor = new Executor();
            Task<bool> task = Task.Run(() => executor.Delete(museum, integration, this));
            deleteButton.IsEnabled = false;
            loadButton.IsEnabled = false;
            bool result = await task;
            if (result)
            {
                AppendTextBox(museum.Name + " was deleted\n");
            }
            else
            {
                AppendTextBox(executor.ErrorMsg + "\n");
            }
            deleteButton.IsEnabled = true;
            loadButton.IsEnabled = true;

        }

        public void AppendTextBox(string value)
        {
            Dispatcher.BeginInvoke((Action)(() => outputTextBox.AppendText(value)));
        }

        /**
        * called by Save button
         * */
        private void SaveMuseumsToFile(object sender, RoutedEventArgs e)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(MuseumDatabase));
                FileStream fs = new FileStream(Constants.MUSEUM_SREIALZED_FILE, FileMode.OpenOrCreate);
                MuseumDatabase database = getDatabase();
                serializer.Serialize(fs, database);
                fs.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong. Try Again!\nException: " + ex.Message);
                return;
            }
            MessageBox.Show("Saved!");

        }

        private void AddMuseum(object sender, RoutedEventArgs e)
        {
            AddMuseumWindow form = new AddMuseumWindow();
            bool? res = form.ShowDialog();
            bool response = false;
            if (null != res)
            {
                response = (bool)res;
            }
            if (response)
            {
                MuseumEntry newEntry = form.AddedMuseum;
                MuseumDatabase database = getDatabase();
                database.Add(newEntry);
            }            
        }

        MuseumDatabase getDatabase()
        {
            return (MuseumDatabase)museumListView.ItemsSource;
        }

        /**
         * Called by minus button
         * */
        private void RemoveMuseum(object sender, RoutedEventArgs e)
        {
            if (museumListView.SelectedItems.Count < 1)
            {
                return;
            }

            MuseumEntry entry = (MuseumEntry)museumListView.SelectedItem;
            MuseumDatabase database = getDatabase();
            database.Remove(entry);
            //museumDataGrid.Rows.Remove(museumDataGrid.SelectedRows[0]);
        }


        public void AppendUpdateText(string value)
        {
            AppendTextBox(value);
        }

    }
}
