

using Amazon.DynamoDBv2.Model;
using LoadTool;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Tool
{

    public class Utils
    {

        readonly static string PREFIX = "gsx$";

        public static string getString(JObject obj, string key)
        {
            string modifiedKey = PREFIX + key;

            JObject keyObject = (JObject)obj[modifiedKey];
            if (null == keyObject)
                return null;

            string retValue = (string)keyObject.GetValue("$t");
            if ("" == retValue)
                retValue = " ";
            return (retValue == null) ? null : retValue;
        }

        public static int getInt(JObject obj, string key)
        {
            string s = getString(obj, key);
            return Int32.Parse(s);
        }

        public static AttributeValue getIntAttributeValue(JObject obj, string key)
        {
            String intValue = getString(obj, key);
            AttributeValue ret = new AttributeValue();
            try
            {
                Int32.Parse(intValue);
            }
            catch (Exception ex)
            {
                ex.Data.Add(Constants.ERROR_FIELD, key);
                throw ex;
            }
            ret.N = intValue;
            return ret;
        }

        public static AttributeValue getBooleanAttributeValue(JObject entry, string key)
        {
            bool boolValue = getBoolean(entry, key);
            AttributeValue value = new AttributeValue();
            value.BOOL = boolValue;
            return value;
        }

        public static AttributeValue getStringAttributeValue(JObject obj, string key)
        {
            String s = getString(obj, key);
            return (null == s) ? null : new AttributeValue((s.Trim().Length == 0) ? s : s.Trim());
        }

        public static AttributeValue getMapAttributeValue(JObject obj, string key)
        {
            AttributeValue value = new AttributeValue();
            Dictionary<string, AttributeValue> map = getMap(obj, key);
            if (null == map)
                return null;
            value.M = map;
            return value;
        }


        public static Dictionary<string, AttributeValue> getMap(JObject obj, string key)
        {
            Dictionary<string, AttributeValue> map = new Dictionary<string, AttributeValue>();
            String englishVersion = getString(obj, key);
            String hebrewVersion = getString(obj, key + ".he");
            String arabicVersion = getString(obj, key + ".ar");
            String chineseVersion = getString(obj, key + ".ch");
            if ((null == englishVersion) && (null == hebrewVersion) && (null == arabicVersion) && (null == chineseVersion))
                return null;
            if ((null != englishVersion) && englishVersion.Trim().Length >= 1)
                map["en"] = new AttributeValue(englishVersion);
            if ((null != hebrewVersion) && hebrewVersion.Trim().Length >= 1)
                map["iw"] = new AttributeValue(hebrewVersion);
            if ((null != arabicVersion) && arabicVersion.Trim().Length >= 1)
                map["ar"] = new AttributeValue(arabicVersion);
            if ((null != chineseVersion) && chineseVersion.Trim().Length >= 1)
                map["ch"] = new AttributeValue(chineseVersion);
            if (map.Count == 0)
                return null;
            return map;
        }

        internal static Dictionary<string, string> getStringMap(JObject obj, string key)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            String englishVersion = getString(obj, key);
            String hebrewVersion = getString(obj, key + ".he");
            String arabicVersion = getString(obj, key + ".ar");
            String chineseVersion = getString(obj, key + ".ch");
            if ((null == englishVersion) && (null == hebrewVersion) && (null == arabicVersion) && (null == chineseVersion))
                return null;
            if ((null != englishVersion) && englishVersion.Trim().Length >= 1)
                map["en"] = englishVersion;
            if ((null != hebrewVersion) && hebrewVersion.Trim().Length >= 1)
                map["iw"] = hebrewVersion;
            if ((null != arabicVersion) && arabicVersion.Trim().Length >= 1)
                map["ar"] = arabicVersion;
            if ((null != chineseVersion) && chineseVersion.Trim().Length >= 1)
                map["ch"] = chineseVersion;
            if (map.Count == 0)
                return null;
            return map;
        }

        internal static Boolean getBoolean(JObject obj, string key)
        {
            String value = getString(obj, key);
            if (null == value) throw new InvalidInputException("No value in " + key);
            if (value.ToLower() == "true")
                return true;
            else if (value.ToLower() == "false")
                return false;
            throw new InvalidInputException("Value not true or false in " + key);
        }

        public static AttributeValue getListAttributeValue(JObject entry, string key)
        {
            String s = getString(entry, key);
            if (null == s)
                return null;
            AttributeValue value = new AttributeValue();
            List<AttributeValue> list = new List<AttributeValue>();
            foreach (String element in s.ToLower().Split(':'))
            {
                list.Add(new AttributeValue(element));
            }
            value.L = list;
            return value;
        }

        public static AttributeValue getNumberSetAttributeValue(JObject entry, string key)
        {
            String s = getString(entry, key);
            if (null == s)
                return null;
            String[] values = s.Split(';');
            AttributeValue ret = new AttributeValue();
            ret.NS = new List<String>(values);
            return ret;
        }

        public static AttributeValue getNodeJsCompatibleSetAttributeValue(JObject entry, string key)
        {
            String s = getString(entry, key);
            if (null == s)
                return null;
            String[] values = s.Split(';');
            AttributeValue ret = new AttributeValue();
            List<AttributeValue> valueList = new List<AttributeValue>();
            foreach (string value in values)
            {
                var attribute = new AttributeValue();
                attribute.N = value;
                valueList.Add(attribute);
            }
            ret.L = valueList;
            return ret;
        }

        public static AttributeValue getFloatAttributeValue(JObject entry, String key)
        {
            String floatValue = getString(entry, key);
            AttributeValue ret = new AttributeValue();
            try
            {
                float.Parse(floatValue);
            }
            catch (Exception ex)
            {
                ex.Data.Add(Constants.ERROR_FIELD, key);
                throw ex;
            }
            ret.N = floatValue;
            return ret;
        }
    }
}