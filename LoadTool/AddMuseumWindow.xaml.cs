﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tool;

namespace LoadTool
{
    /// <summary>
    /// Interaction logic for AddMuseumWindow.xaml
    /// </summary>
    public partial class AddMuseumWindow : Window
    {
        public AddMuseumWindow()
        {
            InitializeComponent();
        }

        string name;
        string id;
        string googledocid;

        public MuseumEntry AddedMuseum
        {
            get
            {
                if (null != name && null != id && null != googledocid) return new MuseumEntry(name, id, googledocid);
                return null;
            }
        }

        private void onOkClick(object sender, RoutedEventArgs e)
        {
            name = nameTextBox.Text;
            id = idTextBox.Text;
            googledocid = googledocTextBox.Text;
            this.DialogResult = true;
        }

        private void onCancelClick(object sender, RoutedEventArgs e)
        {
            name = null;
            id = null;
            googledocid = null;
            this.DialogResult = false;
        }
    }
}
