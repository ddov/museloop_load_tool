

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Tool
{
    public class ScanExporter : Exporter
    {

        private static int PUZZLE_SHEET = 8;

        public ScanExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, Constants.SCAN_TABLE)
        {
        }

        protected override int GetSheetNumber()
        {
            return PUZZLE_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            string IDAttr = Utils.getString(entry, "dbid");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getStringAttributeValue(entry, "vuforia");
            if (null != value)
                item["VuforiaDB"] = value;
            value = Utils.getStringAttributeValue(entry, "a");
            if (null != value)
                item["VuforiaImage"] = value;
            value = Utils.getMapAttributeValue(entry, "q1");
            if (null != value)
                item["Question"] = value;
            value = Utils.getMapAttributeValue(entry, "p1");
            if (null != value)
                item["Popup"] = value;
            return item;
        }

        protected override bool isValid(JObject entry)
        {
            string IDAttr = Utils.getString(entry, "dbid");
            if (mIdSet.Contains(IDAttr))
            {
                throw new Exception(String.Format("DUPLICATE ID: {0}. Check out the " +
                        "Excel file to see there are duplicates in dbid. Aborting!!!!!", IDAttr));
            }
            return base.isValid(entry);
        }

        public override string Name => "Scan";
    }
}
