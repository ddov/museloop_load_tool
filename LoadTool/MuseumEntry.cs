﻿namespace Tool
{
    public class MuseumEntry
    {

        public string Name
        {
            get;
            set;
        }

        public string ID
        {
            get;
            set;
        }

        public string GoogleDocID
        {
            get;
            set;
        }

        public MuseumEntry()
        {

        }

        public MuseumEntry(string name, string id, string googledocid)
        {
            Name = name;
            ID = id;
            GoogleDocID = googledocid;
        }
    }
}