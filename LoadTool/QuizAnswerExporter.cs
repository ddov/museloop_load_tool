﻿using System;
using System.Collections.Generic;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;

namespace Tool
{
    internal class QuizAnswerExporter : Exporter
    {
        
        private static int QUIZ_ANSWER_SHEET = 14;

        public override string Name => Constants.QUIZ_ANSWER_TABLE;

        public QuizAnswerExporter(MuseumEntry museum, AmazonDynamoDBClient client) : base(museum, client, Constants.QUIZ_ANSWER_TABLE)
        {
            
        }

        protected override int GetSheetNumber()
        {
            return QUIZ_ANSWER_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            string IDAttr = Utils.getString(entry, "id");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getMapAttributeValue(entry, "text");
            if (null != value)
                item["text"] = value;
            value = Utils.getStringAttributeValue(entry, "image");
            if (null != value)
                item["image"] = value;
            value = Utils.getBooleanAttributeValue(entry, "correct");
            if (null != value)
                item["correct"] = value;
            return item;
        }
    }
}