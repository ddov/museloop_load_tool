using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Tool
{
    public class ISpyExporter : Exporter
    {
        private static int ISPY_SHEET = 6;
        private static string QUESTION_PREFIX = "q";
        private static string OVERLAY_PREFIX = "a";
        private static string POPUP_PREFIX = "p";

        public ISpyExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, Constants.ISPY_TABLE)
        {
        }

        protected override int GetSheetNumber()
        {
            return ISPY_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            string IDAttr = Utils.getString(entry, "dbid");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getStringAttributeValue(entry, "artjpg");
            if (null != value)
                item["Art"] = value;
            value = getLayers(entry);
            if (null != value)
                item["Layers"] = value;
            value = Utils.getIntAttributeValue(entry, "minsteps");
            if (null != value)
                item["MinSteps"] = value;
            value = Utils.getIntAttributeValue(entry, "stepsfoul");
            if (null != value)
                item["StepsFoul"] = value;
            return item;
        }

        private AttributeValue getLayers(JObject entry)
        {
            List<AttributeValue> layerList = new List<AttributeValue>();
            int layerIndex = 1;
            while (isLayerValid(entry, layerIndex))
            {
                AttributeValue layer = new AttributeValue();
                AttributeValue question = Utils.getMapAttributeValue(entry, QUESTION_PREFIX + layerIndex);
                AttributeValue popup = Utils.getMapAttributeValue(entry, POPUP_PREFIX + layerIndex);
                AttributeValue overlay = Utils.getStringAttributeValue(entry, OVERLAY_PREFIX + layerIndex);
                if (null != question)
                    layer.M.Add("Question", question);
                if (null != popup)
                    layer.M.Add("Popup", popup);
                if (null != overlay)
                    layer.M.Add("Overlay", overlay);
                layerList.Add(layer);
                layerIndex++;
            }
            AttributeValue allLayers = new AttributeValue();
            allLayers.L = layerList;
            return allLayers;
        }

        private bool isLayerValid(JObject entry, int index)
        {
            string question = Utils.getString(entry, QUESTION_PREFIX + index + ".he");
            if (null == question || question.Trim().Length <= 1)
                return false;
            return true;
        }

        protected override bool isValid(JObject entry)
        {
            string IDAttr = Utils.getString(entry, "dbid");
            if (mIdSet.Contains(IDAttr))
            {
                throw new System.Exception(String.Format("DUPLICATE ID: {0}. Check out the " +
                        "Excel file to see there are duplicates in dbid. Aborting!!!!!", IDAttr));
            }
            return base.isValid(entry);
        }

        public override string Name => "ISpy";
    }
}
