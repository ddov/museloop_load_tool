

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Tool
{

    public class PathExporter : Exporter
    {
        private static readonly int PATH_SHEET = 2;

        public PathExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, Constants.PATH_TABLE)
        {
        }

        protected override int GetSheetNumber()
        {
            return PATH_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            value = Utils.getStringAttributeValue(entry, "museumid");
            if (null != value)
                item["MuseumID"] = value;
            string IDAttr = Utils.getString(entry, "dbid");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getMapAttributeValue(entry, "name");
            if (null != value)
                item["Name"] = value;
            value = Utils.getStringAttributeValue(entry, "buildingjpg");
            if (null != value)
                item["Cover"] = value;
            value = Utils.getMapAttributeValue(entry, "instructions");
            if (null != value)
                item["Instructions"] = value;
            value = Utils.getListAttributeValue(entry, "paths");
            if (null != value)
                item["Galleries"] = value;
            value = Utils.getIntAttributeValue(entry, "orderinmuseum");
            if (null != value)
                item["Order"] = value;
            return item;
        }

        protected override bool isValid(JObject entry)
        {
            string IDAttr = Utils.getString(entry, "dbid");
            if (mIdSet.Contains(IDAttr))
            {
                throw new System.Exception(String.Format("DUPLICATE ID: %s. Check out the " +
                        "Excel file to see there are duplicates in dbid. Aborting!!!!!", IDAttr));
            }
            return base.isValid(entry);
        }

        public override string Name => "Path";

    }
}