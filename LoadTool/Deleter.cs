

using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using System;
using System.Collections.Generic;

namespace Tool
{

    public class Deleter
    {

        private readonly AmazonDynamoDBClient mClient;
        public string TargetTable
        {
            get; set;
        }
        private string mMuseumId;
        private string mQueryId;

        private readonly int BATCH_MAX_ITEM_COUNT = 20;

        public Deleter(MuseumEntry entry, AmazonDynamoDBClient client, string targetTable, string queryId, bool integration)
        {
            this.mClient = client;
            TargetTable = (integration) ? getIntegrationTable(targetTable) : targetTable;
            mMuseumId = entry.ID;
            mQueryId = queryId;
        }
        

        private List<Dictionary<string, AttributeValue>> queryItems()
        {

            Dictionary<string, AttributeValue> map = new Dictionary<string, AttributeValue>();
            var MuseumIdAttribute = new AttributeValue();
            MuseumIdAttribute.S = mMuseumId;
            map[":v_id"] = MuseumIdAttribute;

            QueryRequest request = new QueryRequest();
            request.KeyConditionExpression = String.Format("{0} = :v_id", mQueryId);
            request.TableName = TargetTable;
            request.ExpressionAttributeValues = map;

            QueryResponse result = mClient.Query(request);
            return result.Items;
        }

        public void execute()
        {
            List<Dictionary<string, AttributeValue>> items = queryItems();
            int listSize = items.Count;
            int batches = (listSize / BATCH_MAX_ITEM_COUNT) + 1;
            try
            {
                for (int i = 0; i < batches; i++)
                {
                    int howManySent = i * 25;
                    int howManyLeft = listSize - howManySent;
                    List<Dictionary<string, AttributeValue>> sublist = items.GetRange(howManySent, Math.Min(BATCH_MAX_ITEM_COUNT, howManyLeft));
                    sendBatch(sublist);
                }
            }
            catch (Exception th)
            {
                Console.WriteLine(th.StackTrace);
            }
        }

        /**
         * should be no more than 25, otherwise it will fail.
         * @param items
         */
        public void sendBatch(List<Dictionary<string, AttributeValue>> items)
        {
            Dictionary<string, List<WriteRequest>> requestItems = new Dictionary<string, List<WriteRequest>>();
            List<WriteRequest> list = new List<WriteRequest>();
            requestItems[TargetTable] = list;
            foreach (Dictionary<string, AttributeValue> item in items)
            {
                Dictionary<string, AttributeValue> newItem = new Dictionary<string, AttributeValue>();
                if ((TargetTable != Constants.MUSEUM_TABLE) && (TargetTable != getIntegrationTable(Constants.MUSEUM_TABLE)))
                {
                    newItem["MuseumID"] = item["MuseumID"];
                }
                newItem["ID"] = item["ID"];
                list.Add(new WriteRequest(new DeleteRequest(newItem)));
            }
            if (list.Count > 0)
            {
                BatchWriteItemResponse response = mClient.BatchWriteItem(new BatchWriteItemRequest(requestItems));
                Console.WriteLine(response);
            }
            else
            {
                Console.WriteLine("empty list");
            }
        }

        private string getIntegrationTable(string table)
        {
            return (table += "Integration");
        }
    }
}