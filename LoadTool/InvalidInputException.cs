﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadTool
{
    class InvalidInputException : Exception
    {
        public InvalidInputException(String msg) : base(msg) { }

        public InvalidInputException() : base() { }
    }
}
