﻿using System;
using System.Collections.Generic;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;

namespace Tool
{
    public class QuizQuestionExporter : Exporter
    {
     

        private static int QUIZ_QUESTION_SHEET = 13;

        public override string Name => Constants.QUIZ_QUESTION_TABLE;

        public QuizQuestionExporter(MuseumEntry museum, AmazonDynamoDBClient client) : base(museum, client, Constants.QUIZ_QUESTION_TABLE)
        {
        }

        protected override int GetSheetNumber()
        {
            return QUIZ_QUESTION_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            string IDAttr = Utils.getString(entry, "id");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getMapAttributeValue(entry, "title");
            if (null != value)
                item["title"] = value;
            value = Utils.getMapAttributeValue(entry, "question");
            if (null != value)
                item["question"] = value;
            value = Utils.getStringAttributeValue(entry, "image");
            if (null != value)
                item["image"] = value;
            value = Utils.getListAttributeValue(entry, "answers");
            if (null != value)
                item["answers"] = value;
            return item;
        }
    }
}