
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Tool
{
    public class GalleryExporter : Exporter
    {

        private static int GALLERY_SHEET = 3;

        public GalleryExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, Constants.GALLERY_TABLE) { }


        protected override int GetSheetNumber()
        {
            return GALLERY_SHEET;
        }


        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            string IDAttr = Utils.getString(entry, "dbid");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getMapAttributeValue(entry, "pathname");
            if (null != value)
                item["Name"] = value;
            value = Utils.getMapAttributeValue(entry, "location");
            if (null != value)
                item["Location"] = value;
            value = Utils.getStringAttributeValue(entry, "pathcoverjpg");
            if (null != value)
                item["Cover"] = value;
            value = Utils.getMapAttributeValue(entry, "instructions");
            if (null != value)
                item["Instructions"] = value;
            value = Utils.getMapAttributeValue(entry, "alldone");
            if (null != value)
                item["AllDone"] = value;
            value = Utils.getListAttributeValue(entry, "stations");
            if (null != value)
                item["Stations"] = value;
            return item;
        }

        protected override bool isValid(JObject entry)
        {
            string IDAttr = Utils.getString(entry, "dbid");
            if (mIdSet.Contains(IDAttr))
            {
                throw new System.Exception("DUPLICATE ID's in DB. Check out the" +
                        "Excel file to see there are duplicates in dbid");
            }
            return base.isValid(entry);
        }

        public override string Name => "Gallery";
    }
}