﻿using System;
using System.Collections.Generic;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;

namespace Tool
{
    internal class QuizExporter : Exporter
    {
       
        private static int QUIZ_SHEET = 12;

        public override string Name => Constants.QUIZ_TABLE;


        public QuizExporter(MuseumEntry museum, AmazonDynamoDBClient client) : base(museum, client, Constants.QUIZ_TABLE)
        {
            
        }

        protected override int GetSheetNumber()
        {
            return QUIZ_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            item["MuseumID"] = Utils.getStringAttributeValue(entry, "museumid");
            string IDAttr = Utils.getString(entry, "id");
            mIdSet.Add(IDAttr);
            item["ID"] = new AttributeValue(IDAttr);
            value = Utils.getIntAttributeValue(entry, "score");
            if (null != value)
                item["score"] = value;
            value = Utils.getIntAttributeValue(entry, "secondtry");
            if (null != value)
                item["score_second_try"] = value;
            value = Utils.getIntAttributeValue(entry, "maxtrycount");
            if (null != value)
                item["max_try"] = value;
            value = Utils.getListAttributeValue(entry, "questions");
            if (null != value)
                item["questions"] = value;
            return item;
        }
    }
}