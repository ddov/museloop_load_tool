﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tool
{
    class Constants
    {

        public static readonly string ERROR_FIELD = "error_field";
        public static readonly string ERROR_SHEET = "error_sheet";

        public static readonly string MUSEUM_TABLE = "Museums";
        public static readonly string PATH_TABLE = "Paths";
        public static readonly string GALLERY_TABLE = "Galleries";
        public static readonly string STATION_TABLE = "Station";
        public static readonly string GAME_TABLE = "Games";
        public static readonly string CHOOSE_TABLE = "ChooseJS";
        public static readonly string DIFFERENCES_TABLE = "Differences";
        public static readonly string PUZZLE_TABLE = "Puzzle";
        public static readonly string SCAN_TABLE = "Scan";
        public static readonly string FACES_TABLE = "Faces";
        public static readonly string ISPY_TABLE = "ISpy";
        public static readonly string QUIZ_TABLE = "Quiz";
        public static readonly string QUIZ_QUESTION_TABLE = "QuizQuestion";
        public static readonly string QUIZ_ANSWER_TABLE = "QuizAnswer";
        public static readonly string MUSEUM_SREIALZED_FILE = "Museums.xml";
    }
}
