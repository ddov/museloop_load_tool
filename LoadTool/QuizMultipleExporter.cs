﻿using Amazon.DynamoDBv2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tool;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;

namespace LoadTool
{
    class QuizMultipleExporter : MultipleExporter
    {

        private const int QUIZ_GENERAL_SHHET_NUMBER = 12;
        private const int QUIZ_QUESTIONS_SHHET_NUMBER = 13;
        private const int QUIZ_ANSWERS_SHHET_NUMBER = 14;

        private const string TARGET_TABLE = "Quiz";


        public QuizMultipleExporter(MuseumEntry entry,
            AmazonDynamoDBClient client) : base(entry, client, TARGET_TABLE)
        {

        }

        protected override HashSet<int> GetSheetNumbers()
        {
            HashSet<int> set = new HashSet<int>();
            set.Add(QUIZ_GENERAL_SHHET_NUMBER);
            set.Add(QUIZ_QUESTIONS_SHHET_NUMBER);
            set.Add(QUIZ_ANSWERS_SHHET_NUMBER);
            return set;
        }

        protected override List<Dictionary<string, AttributeValue>> createAWSElements(Dictionary<int, List<object>> itemLists)
        {
            throw new NotImplementedException();
        }

        protected override List<object> getItemList(int sheetNumber, JArray entry)
        {
            switch (sheetNumber)
            {
                case QUIZ_GENERAL_SHHET_NUMBER:
                    {
                        return transformToQuizList(entry);
                    }
                case QUIZ_QUESTIONS_SHHET_NUMBER:
                    {
                        return transformToQuestionsList(entry);
                    }
                case QUIZ_ANSWERS_SHHET_NUMBER:
                    {
                        return transformToAnswersList(entry);
                    }
            }

            return null;
        }

        private List<object> transformToAnswersList(JArray array)
        {

            List<Object> list = new List<Object>();
            for (int i = 0; i < array.Count; i++)
            {
                JObject entry = (JObject)array[i];
                if (!isValid(entry))
                    continue;

                QuizAnswer answer = new QuizAnswer();

                String value = Utils.getString(entry, "id");
                if (null == value)
                {
                    throw new InvalidInputException("Id is missing");
                }

                answer.id = value;

                value = Utils.getString(entry, "image");
                answer.image = value;

                bool correctValue = Utils.getBoolean(entry, "correct");
                answer.correct = correctValue;

                Dictionary<String, String> text = Utils.getStringMap(entry, "text");
                if (null == text)
                    throw new InvalidInputException("invalid input in 'text' ");

                answer.text = text;
                list.Add(answer);
            }

            return list;
        }


        private List<object> transformToQuestionsList(JArray array)
        {
            List<Object> list = new List<Object>();
            for (int i = 0; i < array.Count; i++)
            {
                JObject entry = (JObject)array[i];
                if (!isValid(entry))
                    continue;

                QuizQuestion question = new QuizQuestion();

                String id = Utils.getString(entry, "id");
                if (null == id)
                    throw new InvalidInputException("Id is missing");
               

                question.id = id;

                Dictionary<string, string> title = Utils.getStringMap(entry, "title");
                question.title = title;

                Dictionary<string, string> questionText = Utils.getStringMap(entry, "question");
                question.question = questionText;

                String image = Utils.getString(entry, "image");
                question.image = image;

                list.Add(question);
            }

            return list;
        }

        private List<object> transformToQuizList(JArray array)
        {
            List<Object> list = new List<Object>();
            for (int i = 0; i < array.Count; i++)
            {
                JObject entry = (JObject)array[i];
                if (!isValid(entry))
                    continue;

                QuizGeneralEntry quiz = new QuizGeneralEntry();

                String id = Utils.getString(entry, "id");
                if (null == id)
                    throw new InvalidInputException("Id is missing");


                quiz.id = id;

                int score = Utils.getInt(entry, "score");
                quiz.score = score;

                int scoreSecondTry = Utils.getInt(entry, "score_second_try");
                quiz.secondTryScore = scoreSecondTry;

                int maxTryCount = Utils.getInt(entry, "max_try_count");
                quiz.maxTryCount = maxTryCount;

                String questionsText = Utils.getString(entry, "questions");
                List<String> questions = new List<string>();
                foreach (String s in questionsText.Split(':')) {
                    questions.Add(s);
                }
                quiz.questions = questions;

                list.Add(quiz);
            }

            return list;
        }

        class QuizGeneralEntry
        {
            public String museumId;
            public String id;
            public String image;
            public int score;
            public int secondTryScore;
            public int maxTryCount;
            public List<String> questions;
        }

        class QuizQuestion
        {
            public String id;
            public Dictionary<String, String> title;
            public Dictionary<String, String> question;
            public String image;
        }

        class QuizAnswer
        {
            public String id;
            public Dictionary<String, String> text;
            public String image;
            public Boolean correct;
        }
    }
}
