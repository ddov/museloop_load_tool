﻿using Amazon.DynamoDBv2;
using LoadTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tool
{
    class Executor
    {

        static AmazonDynamoDBClient client;
        private string accesskey_id = "AKIAJWEADU5LMCDP45NA";
        private string secret_accesskey = "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";

        public Exception Result
        {
            get; set;
        }
        public string ErrorMsg
        {
            get; set;
        }


        public Executor()
        {
            client = new AmazonDynamoDBClient(accesskey_id, secret_accesskey, Amazon.RegionEndpoint.EUWest1);
        }

        public bool CommunicateDynamoDb(MuseumEntry museum, UpdateControl updateControl)
        {
            Result = null;
            ErrorMsg = String.Empty;
            List<Exporter> exporters = new List<Exporter>();
            exporters.Add(new MuseumExporter(museum, client));
            exporters.Add(new PathExporter(museum, client));
            exporters.Add(new GalleryExporter(museum, client));
            exporters.Add(new StationExporter(museum, client));
            exporters.Add(new ChooseExporter(museum, client));
            exporters.Add(new DifferencesExporter(museum, client));
            exporters.Add(new ISpyExporter(museum, client));
            exporters.Add(new PuzzleExporter(museum, client));
            exporters.Add(new ScanExporter(museum, client));
            exporters.Add(new FacesExporter(museum, client));
            exporters.Add(new QuizExporter(museum, client));
            exporters.Add(new QuizQuestionExporter(museum, client));
            exporters.Add(new QuizAnswerExporter(museum, client));

            try
            {

                foreach (Exporter exporter in exporters)
                {
                    exporter.execute();
                    if (null != updateControl)
                    {
                        updateControl.AppendUpdateText("finished " + exporter.Name + "\n");
                    }
                }
            }
            catch (Exception ex)
            {
                Result = ex;
                StringBuilder sb = new StringBuilder();
                var error_sheet = ex.Data[Constants.ERROR_SHEET];
                var error_field = ex.Data[Constants.ERROR_FIELD];
                if (error_sheet != null)
                {
                    sb.AppendLine("Error in sheet " + error_sheet);
                }
                if (error_field != null)
                {
                    sb.AppendLine("Error in field " + error_field);
                }
                sb.AppendLine(ex.Message);
                ErrorMsg = sb.ToString();
                return false;
            }
            return true;
        }



        public bool Delete(MuseumEntry museum, bool integration, UpdateControl updateControl)
        {

            Result = null;
            ErrorMsg = String.Empty;
            List<Deleter> deleters = new List<Deleter>();
            deleters.Add(new Deleter(museum, client, Constants.MUSEUM_TABLE, "ID", integration));
            deleters.Add(new Deleter(museum, client, Constants.PATH_TABLE, "MuseumID", integration));
            deleters.Add(new Deleter(museum, client, Constants.GALLERY_TABLE, "MuseumID", integration));
            deleters.Add(new Deleter(museum, client, Constants.STATION_TABLE, "MuseumID", integration));
            deleters.Add(new Deleter(museum, client, Constants.CHOOSE_TABLE, "MuseumID", integration));
            deleters.Add(new Deleter(museum, client, Constants.DIFFERENCES_TABLE, "MuseumID", integration));
            deleters.Add(new Deleter(museum, client, Constants.FACES_TABLE, "MuseumID", integration));
            deleters.Add(new Deleter(museum, client, Constants.ISPY_TABLE, "MuseumID", integration));
            deleters.Add(new Deleter(museum, client, Constants.PUZZLE_TABLE, "MuseumID", integration));
            deleters.Add(new Deleter(museum, client, Constants.SCAN_TABLE, "MuseumID", integration));

            try
            {
                foreach (Deleter deleter in deleters)
                {
                    deleter.execute();
                    if (null != updateControl)
                    {
                        updateControl.AppendUpdateText("finished " + deleter.TargetTable + "\n");
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
                Result = ex;
                return false;
            }

            return true;
        }

    }

    public interface UpdateControl
    {
        void AppendUpdateText(string value);
    }
        
}
