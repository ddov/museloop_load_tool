
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Tool
{
    public class GamesExporter : Exporter
    {

        private static int GAMES_SHEET = 5;

        public GamesExporter(MuseumEntry entry, AmazonDynamoDBClient client) : base(entry, client, "Games")
        {
        }

        protected override int GetSheetNumber()
        {
            return GAMES_SHEET;
        }

        protected override Dictionary<string, AttributeValue> jsonToAws(JObject entry)
        {
            AttributeValue value = null;
            Dictionary<string, AttributeValue> item = new Dictionary<string, AttributeValue>();
            value = Utils.getStringAttributeValue(entry, "id");
            if (null != value)
                item["ID"] = value;
            value = Utils.getStringAttributeValue(entry, "rgb");
            if (null != value)
                item["Color"] = value;
            value = Utils.getMapAttributeValue(entry, "gamename");
            if (null != value)
                item["Name"] = value;
            value = Utils.getMapAttributeValue(entry, "instructions");
            if (null != value)
                item["Instructions"] = value;
            value = Utils.getIntAttributeValue(entry, "mistakesbeforewarning");
            if (null != value)
                item["MistakesBeforeWarning"] = value;
            value = Utils.getMapAttributeValue(entry, "warning");
            if (null != value)
                item["Warning"] = value;
            value = Utils.getStringAttributeValue(entry, "icons");
            if (null != value)
                item["Icon"] = value;
            return item;
        }

        protected override bool isValid(JObject entry)
        {
            return true;
        }

        public override string Name => "Games";
    }
}
